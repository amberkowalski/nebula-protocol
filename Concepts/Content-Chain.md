## What is a Content Chain?
A content chain works similar to a blockchain in the sense that each block is linked to the last block. Every post / message sent across a Nebula network is represented by a content chain. The reason for the use of the Content Chain system is to allow modification of the content in the original block while preserving the content.  
Reading a Content Chain is as simple as reading from oldest -> newest blocks and confirming that the chain is in order. The diff system will not work in an improperly-ordered chain.


A block in the content chain stores a few values:
* Hash of the last block
* Timestamp
* Hash of the block's contents as well as the previous values

The following values are optional, only being used if an authority is verifying that a block was handled by them

* Identifier of the hash signer
* Signed hash of the previous hash as well as the hash signer

These values ensure that the content of the block cannot be modified in transit since modification of any of the values results in the block and / or chain breaking. Please note, however, someone could re-write a chain's history and rehash all the values to change the result of the chain, which is why signing is recommended for all blocks.

## Block generation

Content Chains are transmitted in their entirety and the chain is expected to be kept together on a peer's system. The actual generation of a block in a chain is described below using psuedocode.

```Python
# All values are treated as strings during generation and are defined as such
String lastHash; # The hash of the last block
String timestamp; # The timestamp the block was created on
String content; # The content of the block

# If used
String identifier; # Identifier that can be used to find the authority that signed the block's content
String signedValidationHash; # Signed version of the validationHash with the identifier prepended to it.
String privateKey; # The private key of the authority

# Concatenate the values and hash them using SHA256
String validationHash = sha256Hash(lastHash + timestamp + content);

# If used
signedValidationHash = sign(privateKey, identifer + validationHash)


Object block = {
"lasthash": lastHash,
"timestamp": timestamp,
"content": content,
"validationhash": validationHash,

# If an identifier and signed hash are used
"identifier": identifier,
"signedvalidationhash": signedValidationHash
}
```

In this example, block is an object that holds all the information for the block. The blocks are all encoded with json and a Chain is created.

Be aware that all values must be hashed in the correct order.

## Chain generation

A chain is an array of blocks, in order from oldest being 0, to newest. The array is encoded in JSON to create a network-transferable chain.



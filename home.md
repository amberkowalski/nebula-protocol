Welcome to the central page of the Nebula-Protocol repository. Here you can find links to helpful pages, and an explaination for why this repo is a bunch of .md files.


The linked documentation (and other information) is for the most current version of the protocol (beta). All information here is relevant and should be treated as such. Since the protcol is currently in beta, this documentation is not final.

The repo is a collection of md files to ensure all changes to the protocol are version-controlled. If you'd like to suggest a better way (of which there are most likely many) please submit an issue.

# Suggested Reading
If you're new here, here are some pages you may be interested in reading.
*pages*

# Documentation
*links to documentation*

# Main Contributors
@amberkowalski - Creator and **the** contributor

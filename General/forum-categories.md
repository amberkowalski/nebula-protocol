# Why use categories?
Categories are intented to make a forum's topic / purpose more obvious. For instance, consider two forums, `gaming.rust` and `programming.rust`. They both have the same name, but are about two very different things. Categories are a much prettier version of having forums named `rustgame` or `rustlang`. There are also special categories that have a specific purpose.

# Current official categories
* politics - **SPECIAL CATEGORY**: According to the CoC,  this is the only place political discussion and any discussion where there are two or more opposing parties is allowed to happen. Please note jokes of political origin are also only allowed here. Please refer to the CoC for any additional rules regarding this category.
* gaming - Anything about gaming
* anime - Anything about anime
* lang - Any forum about a spoken / written language
* programming - Anything about programming
* nebula - Anything about the Nebula protocol
* os - Anything about operating systems
* opensource - Open source development / Open source programs
* cars - Anything about cars
* hobby - Anything about hobbies that don't have their own category
* talk - A place for forums that are mainly about having conversations with eachother

If you believe another category should be added, please submit an issue with why!

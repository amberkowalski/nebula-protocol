# How does a network using the Nebula protocol operate?
A network that uses the Nebula protocol has 3 different types of peers: peers, ultrapeers, and nodes. The reason the network is split up in this way is to try and minimize congestion.
### Peers
A peer is a user who has few requirements. A peer can only connect to ultrapeers and must use them to communicate with the rest of the network.  
* They can subscribe to a forum by informing the ultrapeers its connected to that it would like to recieve content from a forum
* They send requests for content (identified by a hash or other unique identifer) to its ultrapeers who will contact their peers to attempt to find it
* They answer requests for content by initiating a connection to the requester (using a node as a proxy if required) and sending it to them
### Ultrapeers
Ultrapeers also act as a normal peer for the user who chooses to be one. In addition to the functions of a peer, they also have some benefits for ultrapeering, at the cost of a bit of bandwidth.
* May communicate directly with nodes, electing to subscribe to certain forums. They are required to subscribe to every forum that their children request in order to ensure the children recieve the content they've subscribed to.
* Communicate with other ultrapeers. This can result in on ultrapeer subscribing to content from a node, and other ultrapeers subscribing to that ultrapeer. This setup reduces load on nodes.
* Communicate with peers. Ultrapeers are a peer's only way to access the network without them becoming an ultrapeer themself.

 

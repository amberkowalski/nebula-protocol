# What is Nebula?
At its core, Nebula is a protocol designed to facilitate transfer information between a network of computers. Nebula's main purpose is to transfer posts, comments, and messages between computers to create a network that functions as a forum system. It should be noted that the purpose of nebula is not to transfer binaries, or any file larger than 2MiB. Although a large forum system can operate using the Nebula protocol, Nebula is **not** a forum platform. 

# Why was Nebula created?
Nebula is designed to be a protocol that prevents censorship of content that uses the network. This is accomplished with the identity system and Content Chains. A post can be modified using the Content Chain system, but it must always contain the content of the original post for the chain to be valid. The only way to prevent something from spreading on the network is if nearly all users decide they don't want to redistribute it. Because there is no central agency that controls the content distributed through Nebula, there is no corporation, government, or other entity that can directly influence the content on the network. The only rules that apply to the Nebula network are listed in the Code of Conduct, and are not enforced by a central agency.

# If there's no central agency, why is there a Code of Conduct?
Additionally, how is it enforced?


There is no central agency to enforce it, therefore there is no complete way to enforce it. Everyone using the network are encouraged to follow the Code of Conduct, and enforce it in the only way they can. The CoC does not prevent peers and ultrapeers from filtering what content they redistribute, so they are encouraged to filter out identities and posts that are in blatant violation. (Please note, however, Nodes are encouraged to not filter posts that don't have authority over) It is the job of every user to keep content distributed with the Nebula protocol in line with the CoC, both by following it themselves, and stopping the spread of content that is in violation.

# Important Terms
* Identity - A collection of information that is sent with content to show it originated a source
* Content Chain - A system that allows content to be modified, keeping track of previous modifications

# Contribution
Feel free to contribute! Make sure you're using the correct development repo and make PRs for any changes you think should be made.  
